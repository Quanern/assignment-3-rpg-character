package no.experis.factories;

import no.experis.gear.weapon.MagicWeapon;
import no.experis.gear.weapon.MeleeWeapon;
import no.experis.gear.weapon.RangedWeapon;
import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponType;

//Factory to create new weapons.

public class WeaponFactory {
    public Weapon getWeapon(String name, WeaponType weaponType, int level){
        switch (weaponType){
            case Magic:
                return new MagicWeapon(name, level);
            case Ranged:
                return new RangedWeapon(name, level);
            case Melee:
                return new MeleeWeapon(name, level);
            default:
                return null;
        }
    }
}
