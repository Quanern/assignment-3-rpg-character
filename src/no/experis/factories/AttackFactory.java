package no.experis.factories;

import no.experis.attack.Attack;
import no.experis.hero.abstractions.Hero;

//Factory to initiate new attacks.

public class AttackFactory {
    public Attack getAttack(Hero hero){
        return new Attack(hero);
    }
    public Attack getAttack(Hero hero, Hero target){
        return new Attack(hero, target);
    }
}
