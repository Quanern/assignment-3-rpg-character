package no.experis.factories;

import no.experis.gear.armor.Cloth;
import no.experis.gear.armor.Leather;
import no.experis.gear.armor.Plate;
import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.armor.abstractions.ArmorType;

//Factory to create new armor pieces.

public class ArmorFactory {
    public Armor getArmor (String name, ArmorType armorType, ArmorSlot armorSlot, int level){
        switch(armorType){
            case Plate:
                return new Plate(name, armorSlot, level);
            case Leather:
                return new Leather(name, armorSlot, level);
            case Cloth:
                return new Cloth(name, armorSlot, level);
            default:
                return null;
        }
    }
}
