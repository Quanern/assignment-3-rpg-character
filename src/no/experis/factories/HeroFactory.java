package no.experis.factories;

import no.experis.hero.*;
import no.experis.hero.abstractions.Hero;
import no.experis.hero.abstractions.HeroClass;

//Factory to create new Heroes.

public class HeroFactory {
    public Hero getHero(String name, HeroClass heroClass) {
        switch(heroClass){
            case Mage:
                return new Mage(name);
            case Ranger:
                return new Ranger(name);
            case Warrior:
                return new Warrior(name);
            default:
                return null;

        }
    }
}
