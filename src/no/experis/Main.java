package no.experis;

import no.experis.attack.Attack;
import no.experis.factories.ArmorFactory;
import no.experis.factories.AttackFactory;
import no.experis.factories.HeroFactory;
import no.experis.factories.WeaponFactory;
import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.armor.abstractions.ArmorType;
import no.experis.hero.equipment.Equipment;
import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponSlot;
import no.experis.gear.weapon.abstractions.WeaponType;
import no.experis.hero.abstractions.Hero;
import no.experis.hero.abstractions.HeroClass;
import no.experis.hero.equipment.EquipmentGetter;

public class Main {
    private static void print(){
        System.out.println("---------------------------------------------------------------");
    }

    public static void main(String[] args) {
        HeroFactory heroFactory = new HeroFactory();
        ArmorFactory armorFactory = new ArmorFactory();
        WeaponFactory weaponFactory = new WeaponFactory();
        AttackFactory attackFactory = new AttackFactory();
        Equipment equip = new Equipment();
        EquipmentGetter equipment = new EquipmentGetter();

        //Demonstration
        //Creating heroes
        print();
        System.out.println("Creating heroes");
        Hero quan = heroFactory.getHero("Quan, World's Edge", HeroClass.Warrior);
        Hero edgy = heroFactory.getHero("Sk8trboi", HeroClass.Warrior);
        Hero mage = heroFactory.getHero("Master Mage", HeroClass.Mage);
        Hero legolas = heroFactory.getHero("Legolas of the Woodland Realms", HeroClass.Ranger);
        System.out.println(quan);
        System.out.println(edgy);
        System.out.println(mage);
        System.out.println(legolas);

        //Creating armor
        print();
        System.out.println("Creating armor");
        Armor simpleRobe = armorFactory.getArmor("Simple Robe", ArmorType.Cloth, ArmorSlot.Body, 1);
        Armor simplePants = armorFactory.getArmor("Simple Pants", ArmorType.Cloth, ArmorSlot.Legs, 1);
        Armor simpleHat = armorFactory.getArmor("Simple Hat", ArmorType.Cloth, ArmorSlot.Head, 1);
        Armor robe = armorFactory.getArmor("Robe of the Archmagi", ArmorType.Cloth, ArmorSlot.Body, 50);
        Armor hat = armorFactory.getArmor("Cone Hat of the Thousand Dummies", ArmorType.Cloth, ArmorSlot.Head, 50);
        Armor plateHelm5 = armorFactory.getArmor("Plate Helm", ArmorType.Plate, ArmorSlot.Head, 5);
        Armor plateBody5 = armorFactory.getArmor("Chestplate", ArmorType.Plate, ArmorSlot.Body, 5);
        Armor plateHelm1 = armorFactory.getArmor("Plate Helm", ArmorType.Plate, ArmorSlot.Head, 1);
        Armor plateBody1 = armorFactory.getArmor("Chestplate", ArmorType.Plate, ArmorSlot.Body, 1);
        Armor leatherLegs1 = armorFactory.getArmor("Skintight Leather Jeans", ArmorType.Leather, ArmorSlot.Legs,1);
        Armor leatherbody1 = armorFactory.getArmor("Tight Leather Vest", ArmorType.Leather, ArmorSlot.Body, 1);
        Armor leatherLegs6 = armorFactory.getArmor("Skintight Leather Jeans", ArmorType.Leather, ArmorSlot.Legs,6);
        Armor leatherbody6 = armorFactory.getArmor("Tight Leather Vest", ArmorType.Leather, ArmorSlot.Body, 6);
        System.out.println(simpleRobe);
        System.out.println(simpleHat);
        System.out.println(simplePants);
        System.out.println(robe);
        System.out.println(hat);
        System.out.println(plateHelm1);
        System.out.println(plateHelm5);
        System.out.println(plateBody1);
        System.out.println(plateBody5);
        System.out.println(leatherbody1);
        System.out.println(leatherbody6);
        System.out.println(leatherLegs1);
        System.out.println(leatherLegs6);

        //Creating weapons
        print();
        System.out.println("Creating weapons");
        Weapon simpleBow = weaponFactory.getWeapon("Simple Bow", WeaponType.Ranged, 1);
        Weapon heavyAxe = weaponFactory.getWeapon("Blessed Axe of the Windbreaker", WeaponType.Melee, 20);
        Weapon flimsyWand = weaponFactory.getWeapon("Flimsy Wand", WeaponType.Magic, 5);
        System.out.println(simpleBow);
        System.out.println(heavyAxe);
        System.out.println(flimsyWand);

        //Giving exp to a hero
        print();
        System.out.println("Giving xp to a hero");
        System.out.println(quan);
        quan.giveExp(100);
        mage.giveExp(1000);
        System.out.println(quan);
        System.out.println(mage);
        legolas.giveExp(1000000000);
        System.out.println(legolas);

        //Equipping gear
        print();
        System.out.println("Equipping gear to a hero");
        System.out.println(equip.equip(quan, robe));
        System.out.println(equip.equip(quan, heavyAxe));
        System.out.println(legolas);
        System.out.println(equip.equip(legolas, robe));
        System.out.println(legolas);
        System.out.println(equip.equip(legolas, simpleRobe));
        System.out.println(legolas);
        System.out.println(equip.equip(legolas, robe));
        System.out.println(legolas);
        System.out.println(equip.equip(legolas,heavyAxe));

        //Getting information about gear and unequipping
        print();
        System.out.println("Getting information about gear and unequipping");
        System.out.println(equipment.getEquipment(quan));
        System.out.println(equipment.getEquipment(legolas));
        System.out.println(equipment.getEquipment(legolas,ArmorSlot.Head));
        System.out.println(equipment.getEquipment(legolas,ArmorSlot.Body));
        System.out.println(equipment.getEquipment(quan,WeaponSlot.Weapon));
        System.out.println(equipment.getEquipment(legolas,WeaponSlot.Weapon));
        System.out.println(equip.unequip(legolas, ArmorSlot.Head));
        System.out.println(legolas);
        System.out.println(equip.unequip(legolas, ArmorSlot.Body));
        System.out.println(legolas);
        System.out.println(equip.unequip(quan, WeaponSlot.Weapon));


        //Attacking
        print();
        System.out.println("Attacking");
        Attack fail = attackFactory.getAttack(quan);
        equip.equip(quan, simpleBow);
        Attack dummy = attackFactory.getAttack(legolas);
        Attack damage = attackFactory.getAttack(quan, legolas);
        Attack kill = attackFactory.getAttack(legolas, mage);
        Attack dead = attackFactory.getAttack(mage, legolas);
        Attack self = attackFactory.getAttack(legolas, legolas);
        System.out.println(fail);
        System.out.println(dummy);
        System.out.println(damage);
        System.out.println(kill);
        System.out.println(mage);
        System.out.println(dead);
        System.out.println(self);
    }
}
