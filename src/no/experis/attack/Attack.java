package no.experis.attack;

import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponSlot;
import no.experis.gear.weapon.abstractions.WeaponType;
import no.experis.hero.abstractions.Hero;
import no.experis.stats.AttackTypeMultiplier;

public class Attack {
    private Hero hero; //The hero attacking.
    private Hero target; //The hero getting attacked.
    private int damageDone; //Damage done.
    private String attackType; //Type of damage the hero is doing, magic, ranged or melee.
    /**
     * Constructor to create attacks.
     * @param hero The hero that is doing the attacking.
     */
    public Attack(Hero hero){
        this.hero=hero;
        attack();
    }

    public Attack(Hero hero, Hero target){
        this.hero=hero;
        this.target=target;
        if(hero != target){
            attack(target);
        }
    }

    /**
     * Function for a hero to attack. Depending on the type of damage the weapon does, different calculations will
     * happen. If there is no weapon equipped, no damage will be done.
     * @return A String telling what happened when the hero attacked.
     */
    private int attack(){
        int damage=0;
        //Checks if the hero has a weapon equipped.
        if(!hero.getWeaponEquipment().isEmpty()){
            Weapon weapon = hero.getWeaponEquipment().get(WeaponSlot.Weapon);
            WeaponType weaponType = weapon.getWeaponType();
            //Depending on the weapon type, what stats is used for the damage is different.
            switch(weaponType){
                case Melee:
                    attackType = "melee";
                    damage = (int) (weapon.getTotalDamage() +
                            AttackTypeMultiplier.MELEE_ATTACK_MULTIPLIER * hero.getTotalStrength());
                    break;
                case Ranged:
                    attackType = "ranged";
                    damage = weapon.getTotalDamage() +
                            AttackTypeMultiplier.RANGED_ATTACK_MULTIPLIER * hero.getTotalDexterity();
                    break;
                case Magic:
                    attackType = "magic";
                    damage = weapon.getTotalDamage() +
                            AttackTypeMultiplier.MAGIC_ATTACK_MULTIPLIER * hero.getTotalIntelligence();
                    break;
            }
        }
        damageDone = damage;
        return damage;
    }

    /**
     * Reduces the target hero's current health equally to the attack.
     * @param target The hero you want to attack.
     */
    private void attack(Hero target){
        target.setCurrentHealth(target.getCurrentHealth() - attack());
    }

    @Override
    public String toString() {
        if(hero.getCurrentHealth()<1){
            return "You can't attack when you're dead, " + hero.getName();
        }
        if(this.hero == this.target){
            return "You can not attack yourself, " + hero.getName();
        }
        if(damageDone == 0){
            return "You must have a weapon to attack, " + hero.getName();
        }
        if(target != null){
            if(target.getCurrentHealth()<1){
                return hero.getName() +" did " + damageDone +" "+ attackType + " damage to " + target.getName() + ".\n"+
                        target.getName() + " ran out of hp. The hero is dead.";
            }
            return hero.getName() +" did " + damageDone +" "+ attackType + " damage to " + target.getName()
                    +"\n" + target.getName() + " has " + target.getCurrentHealth() + " hp remaining.";
        }
        return hero.getName() +" did " + damageDone +" "+ attackType + " damage.";
    }

}