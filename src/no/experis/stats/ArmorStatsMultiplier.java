package no.experis.stats;
//Multiplier for armor stats.
public class ArmorStatsMultiplier {
    public static final double BODY_MULTIPLIER = 1;
    public static final double HEAD_MULTIPLIER = 0.8;
    public static final double LEGS_MULTIPLIER = 0.6;
}
