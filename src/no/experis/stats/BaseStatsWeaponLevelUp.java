package no.experis.stats;

public class BaseStatsWeaponLevelUp {
    public static final int MELEE_LEVEL_UP = 2;
    public static final int RANGED_LEVEL_UP = 3;
    public static final int MAGIC_LEVEL_UP = 2;
}
