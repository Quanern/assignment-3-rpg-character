package no.experis.stats;

public class BaseStatsArmorLevelUp {
    public static final int CLOTH_ARMOR_LEVEL_UP_HP = 5;
    public static final int CLOTH_ARMOR_LEVEL_UP_STR = 0;
    public static final int CLOTH_ARMOR_LEVEL_UP_DEX = 1;
    public static final int CLOTH_ARMOR_LEVEL_UP_INT = 2;

    public static final int LEATHER_ARMOR_LEVEL_UP_HP = 8;
    public static final int LEATHER_ARMOR_LEVEL_UP_STR = 1;
    public static final int LEATHER_ARMOR_LEVEL_UP_DEX = 2;
    public static final int LEATHER_ARMOR_LEVEL_UP_INT = 0;

    public static final int PLATE_ARMOR_LEVEL_UP_HP = 12;
    public static final int PLATE_ARMOR_LEVEL_UP_STR = 2;
    public static final int PLATE_ARMOR_LEVEL_UP_DEX = 1;
    public static final int PLATE_ARMOR_LEVEL_UP_INT = 0;
}
