package no.experis.stats;

public class BaseStatsHeroLevelUp {
    //Warrior
    public static final int WARRIOR_HEALTH_LEVEL_UP = 30;
    public static final int WARRIOR_STRENGTH_LEVEL_UP = 5;
    public static final int WARRIOR_DEXTERITY_LEVEL_UP = 2;
    public static final int WARRIOR_INTELLIGENCE_LEVEL_UP = 1;
    //Ranger
    public static final int RANGER_HEALTH_LEVEL_UP = 20;
    public static final int RANGER_STRENGTH_LEVEL_UP = 2;
    public static final int RANGER_DEXTERITY_LEVEL_UP = 5;
    public static final int RANGER_INTELLIGENCE_LEVEL_UP = 1;
    //Mage
    public static final int MAGE_HEALTH_LEVEL_UP = 15;
    public static final int MAGE_STRENGTH_LEVEL_UP = 1;
    public static final int MAGE_DEXTERITY_LEVEL_UP = 2;
    public static final int MAGE_INTELLIGENCE_LEVEL_UP = 5;
}
