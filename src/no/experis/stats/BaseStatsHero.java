package no.experis.stats;

public class BaseStatsHero {
    //Warrior
    public static final int WARRIOR_BASE_HEALTH = 150;
    public static final int WARRIOR_STRENGTH = 10;
    public static final int WARRIOR_DEXTERITY = 3;
    public static final int WARRIOR_INTELLIGENCE = 1;
    //Ranger
    public static final int RANGER_BASE_HEALTH = 120;
    public static final int RANGER_STRENGTH = 5;
    public static final int RANGER_DEXTERITY = 10;
    public static final int RANGER_INTELLIGENCE = 2;
    //Mage
    public static final int MAGE_BASE_HEALTH = 100;
    public static final int MAGE_STRENGTH = 2;
    public static final int MAGE_DEXTERITY = 3;
    public static final int MAGE_INTELLIGENCE = 10;
}
