package no.experis.stats;

public class BaseStatsWeapon {
    public static final int MELEE_BASE_DAMAGE = 15;
    public static final int RANGED_BASE_DAMAGE = 5;
    public static final int MAGIC_BASE_DAMAGE = 25;
}
