package no.experis.stats;

public class BaseStatsArmor {
    public static final int CLOTH_ARMOR_BASE_HP = 10;
    public static final int CLOTH_ARMOR_BASE_STR = 0;
    public static final int CLOTH_ARMOR_BASE_DEX = 1;
    public static final int CLOTH_ARMOR_BASE_INT = 3;

    public static final int LEATHER_ARMOR_BASE_HP = 20;
    public static final int LEATHER_ARMOR_BASE_STR = 1;
    public static final int LEATHER_ARMOR_BASE_DEX = 3;
    public static final int LEATHER_ARMOR_BASE_INT = 0;

    public static final int PLATE_ARMOR_BASE_HP = 30;
    public static final int PLATE_ARMOR_BASE_STR = 3;
    public static final int PLATE_ARMOR_BASE_DEX = 1;
    public static final int PLATE_ARMOR_BASE_INT = 0;
}
