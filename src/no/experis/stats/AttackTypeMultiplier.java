package no.experis.stats;

public class AttackTypeMultiplier {
    public static final double MELEE_ATTACK_MULTIPLIER = 1.5;
    public static final int RANGED_ATTACK_MULTIPLIER = 2;
    public static final int MAGIC_ATTACK_MULTIPLIER = 3;
}
