package no.experis.gear.weapon;

import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponType;
import no.experis.stats.BaseStatsWeapon;
import no.experis.stats.BaseStatsWeaponLevelUp;

public class MeleeWeapon extends Weapon {
    private static WeaponType weaponType = WeaponType.Melee; //Sets the weaponType to melee.
    /**
     * Constructor to make a new Melee Weapon.
     * @param name Name of the Melee weapon.
     * @param level level of the weapon for bonus damage.
     */
    public MeleeWeapon(String name, int level) {
        super(name, weaponType, level);
        setBaseDamage();
        setBonusDamage();
        setTotalDamage();
    }
    /**
     * Sets the base damage for the weapon
     */
    private void setBaseDamage(){
        baseDamage = BaseStatsWeapon.MELEE_BASE_DAMAGE;
    }

    /**
     * Sets the bonus damage of the weapon, dependent on the level.
     */
    private void setBonusDamage(){
        bonusDamage = BaseStatsWeaponLevelUp.MELEE_LEVEL_UP * (getLevel() - 1);
    }

    /**
     * Sets the total damage of the weapon by adding base and bonus damage.
     */
    private void setTotalDamage(){
        totalDamage = baseDamage + bonusDamage;
    }
}

