package no.experis.gear.weapon;

import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponType;
import no.experis.stats.BaseStatsWeapon;
import no.experis.stats.BaseStatsWeaponLevelUp;

public class RangedWeapon  extends Weapon {
    private static WeaponType weaponType = WeaponType.Ranged; //Sets the weaponType to ranged.
    /**
     * Constructor to make a new Ranged Weapon.
     * @param name Name of the Ranged weapon.
     * @param level level of the weapon for bonus damage.
     */
    public RangedWeapon(String name, int level) {
        super(name, weaponType, level);
        setBaseDamage();
        setBonusDamage();
        setTotalDamage();
    }

    /**
     * Sets the base damage for the weapon
     */
    private void setBaseDamage(){
        baseDamage = BaseStatsWeapon.RANGED_BASE_DAMAGE;
    }

    /**
     * Sets the bonus damage of the weapon, dependent on the level.
     */
    private void setBonusDamage(){
        bonusDamage = BaseStatsWeaponLevelUp.RANGED_LEVEL_UP * (getLevel() - 1);
    }

    /**
     * Sets the total damage of the weapon by adding base and bonus damage.
     */
    private void setTotalDamage(){
        totalDamage = baseDamage + bonusDamage;
    }
}
