package no.experis.gear.weapon;

import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponType;
import no.experis.stats.BaseStatsWeapon;
import no.experis.stats.BaseStatsWeaponLevelUp;

public class MagicWeapon extends Weapon {
    private static WeaponType weaponType = WeaponType.Magic; //Sets the weaponType to magic.
    /**
     * Constructor to make a new Magic Weapon.
     * @param name Name of the Magic weapon.
     * @param level level of the weapon for bonus damage.
     */
    public MagicWeapon(String name, int level) {
        super(name, weaponType, level);
        setBaseDamage();
        setBonusDamage();
        setTotalDamage();
    }
    /**
     * Sets the base damage for the weapon
     */
    private void setBaseDamage(){
        baseDamage = BaseStatsWeapon.MAGIC_BASE_DAMAGE;
    }

    /**
     * Sets the bonus damage of the weapon, dependent on the level.
     */
    private void setBonusDamage(){
        bonusDamage = BaseStatsWeaponLevelUp.MAGIC_LEVEL_UP * (getLevel() - 1);
    }

    /**
     * Sets the total damage of the weapon by adding base and bonus damage.
     */
    private void setTotalDamage(){
        totalDamage = baseDamage + bonusDamage;
    }
}


