package no.experis.gear.weapon.abstractions;

public abstract class Weapon {
    private String name; //Name of the weapon.
    private WeaponType weaponType; //What kind of weapon it is, magic, melee, ranged.
    protected int baseDamage; //The base damage of the weapon, varies with what kind of weapon it is.
    protected int bonusDamage; //Bonus damage the weapon gains from being higher than level 1, varies with weaponType.
    protected int totalDamage; //Sum of bonus and base damage.
    private int level; //The level of the weapon.

    /**
     * Constructor for a new weapon. Takes the name, weaponType and level as parameters so it can calculate the
     * weapon's stats.
     * @param name The name of the weapon to be created.
     * @param weaponType The type of the weapon to be created; magic, melee or ranged.
     * @param level The level of the weapon to be created.
     */
    public Weapon(String name, WeaponType weaponType, int level) {
        this.name = name;
        this.weaponType = weaponType;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getTotalDamage() {
        return totalDamage;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Weapon stats for:\t" + name +
                "\nWeapon type:\t" + weaponType +
                "\nBase damage:\t" + baseDamage +
                "\nBonus damage:\t" + bonusDamage +
                "\nTotal damage:\t" + totalDamage +
                "\nLevel:\t" + level;
    }
}
