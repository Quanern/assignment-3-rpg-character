package no.experis.gear.weapon.abstractions;
//What kind of weapon each weapon is.
public enum WeaponType {
    Melee, Ranged, Magic
}
