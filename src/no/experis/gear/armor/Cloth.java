package no.experis.gear.armor;

import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.armor.abstractions.ArmorType;
import no.experis.gear.armor.abstractions.Stats;
import no.experis.stats.BaseStatsArmor;
import no.experis.stats.BaseStatsArmorLevelUp;

public class Cloth extends Armor implements Stats {
    private static ArmorType armorType = ArmorType.Cloth; //Sets the armorType to Cloth

    /**
     * Constructor to create a new Cloth armor piece.
     * @param name name of the Cloth armor
     * @param armorSlot which armor slot the armor fits into
     * @param level the level of the armor for bonus stats
     */
    public Cloth(String name, ArmorSlot armorSlot, int level){
        super(name, armorType, armorSlot, level);
        setMultiplier(armorSlot);
        setBaseStats();
        setBonusStats();
        setTotalStats();
    }

    /**
     * Sets the base stats for cloth armor
     */
    public void setBaseStats(){
        baseHealth = BaseStatsArmor.CLOTH_ARMOR_BASE_HP;
        baseStrength = BaseStatsArmor.CLOTH_ARMOR_BASE_STR;
        baseDexterity = BaseStatsArmor.CLOTH_ARMOR_BASE_DEX;
        baseIntelligence = BaseStatsArmor.CLOTH_ARMOR_BASE_INT;
    }

    /**
     * Sets the bonus stats for CLOTH armor.
     */
    public void setBonusStats(){
        int addedStatsLevels = (level-1);
        bonusHealth =  addedStatsLevels * BaseStatsArmorLevelUp.CLOTH_ARMOR_LEVEL_UP_HP;
        bonusStrength = addedStatsLevels * BaseStatsArmorLevelUp.CLOTH_ARMOR_LEVEL_UP_STR;
        bonusDexterity = addedStatsLevels *  BaseStatsArmorLevelUp.CLOTH_ARMOR_LEVEL_UP_DEX;
        bonusIntelligence = addedStatsLevels * BaseStatsArmorLevelUp.CLOTH_ARMOR_LEVEL_UP_INT;
    }

    @Override
    public String toString() {
        return "Item stats for: " + name +
                "\nArmor Type:\t" + armorType +
                "\nSlot:\t\t" + armorSlot +
                "\nBonus HP:\t" + totalHealth +
                "\nBonus DEX:\t" + totalDexterity +
                "\nBonus INT:\t" + totalIntelligence +
                "\nLevel:\t\t" + level;
    }
}
