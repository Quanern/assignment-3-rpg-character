package no.experis.gear.armor;

import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.armor.abstractions.ArmorType;
import no.experis.gear.armor.abstractions.Stats;
import no.experis.stats.BaseStatsArmor;
import no.experis.stats.BaseStatsArmorLevelUp;

public class Plate extends Armor implements Stats {
    private static ArmorType armorType = ArmorType.Plate; //Sets the armorType to Plate


    /**
     * Constructor to create a new Plate armor piece.
     * @param name name of the Plate armor
     * @param armorSlot which armor slot the armor fits into
     * @param level the level of the armor for bonus stats
     */
    public Plate(String name, ArmorSlot armorSlot, int level){
        super(name, armorType, armorSlot, level);
        setMultiplier(armorSlot);
        setBaseStats();
        setBonusStats();
        setTotalStats();
    }

    /**
     * Sets the base stats for PLATE armor
     */
    public void setBaseStats(){
        baseHealth = BaseStatsArmor.PLATE_ARMOR_BASE_HP;
        baseStrength = BaseStatsArmor.PLATE_ARMOR_BASE_STR;
        baseDexterity = BaseStatsArmor.PLATE_ARMOR_BASE_DEX;
        baseIntelligence = BaseStatsArmor.PLATE_ARMOR_BASE_INT;
    }

    /**
     * Sets the bonus stats for PLATE armor.
     */
    public void setBonusStats(){
        int addedStatsLevels = (level-1);
        bonusHealth =  addedStatsLevels * BaseStatsArmorLevelUp.PLATE_ARMOR_LEVEL_UP_HP;
        bonusStrength = addedStatsLevels * BaseStatsArmorLevelUp.PLATE_ARMOR_LEVEL_UP_STR;
        bonusDexterity = addedStatsLevels *  BaseStatsArmorLevelUp.PLATE_ARMOR_LEVEL_UP_DEX;
        bonusIntelligence = addedStatsLevels * BaseStatsArmorLevelUp.PLATE_ARMOR_LEVEL_UP_INT;
    }
    @Override
    public String toString() {
        return "Item stats for: " + name +
                "\nArmor Type:\t" + armorType +
                "\nSlot:\t\t" + armorSlot +
                "\nBonus HP:\t" + totalHealth +
                "\nBonus STR:\t" + totalStrength +
                "\nBonus DEX:\t" + totalDexterity +
                "\nLevel:\t\t" + level;
    }
}

