package no.experis.gear.armor.abstractions;

import no.experis.stats.ArmorStatsMultiplier;

public abstract class Armor {
    private ArmorType armorType; //The types of armor available.
    protected ArmorSlot armorSlot; //The slots armor can go into available.
    protected String name; //Name of the armor.
    protected int totalHealth; //Total stats the armor gives.
    protected int totalStrength;
    protected int totalDexterity;
    protected int totalIntelligence;
    protected int baseHealth; //Base stats of the armor.
    protected int baseStrength;
    protected int baseDexterity;
    protected int baseIntelligence;
    protected int bonusHealth; //Bonus stats the armor has from being higher than level 1.
    protected int bonusStrength;
    protected int bonusDexterity;
    protected int bonusIntelligence;
    protected int level; //The level of the armor.
    private double multiplier; //Multiplier for stats depending on armorSlot.

    /**
     * Constructor for the armor. Takes in the parameters name, armorType (Cloth, Leather, Plate)
     * armorslot (Head, Body, Legs) and level of the gear. Depending on the armor type the gear will have different
     * base stats and stat gains when leveling up, and depending on the armor slot the gear's stats will scale.
     * @param name Name of the piece of armor.
     * @param armorType What material the armor is made of, Cloth, Leather or Plate.
     * @param armorSlot What slot the armor fits in, Body, Head or Legs.
     * @param level The level of the armor, minimum level a hero has to be to equip it and stat bonuses to the armor.
     */
    public Armor(String name, ArmorType armorType, ArmorSlot armorSlot, int level){
        this.name = name;
        this.armorType = armorType;
        this.armorSlot = armorSlot;
        this.level = level;

    }

    /**
     * Sets the multiplier for the gear's stats depending on it's armor slot.
     * @param armorSlot The armor slot that the armor fits into. Depending on the slot the multiplier will be different.
     */
    protected void setMultiplier(ArmorSlot armorSlot){
        switch(armorSlot){
            case Body:
                multiplier = ArmorStatsMultiplier.BODY_MULTIPLIER;
                break;
            case Head:
                multiplier = ArmorStatsMultiplier.HEAD_MULTIPLIER;
                break;
            case Legs:
                multiplier = ArmorStatsMultiplier.LEGS_MULTIPLIER;
                break;
            default:
                multiplier = 0;
                break;
        }
    }

    /**
     * Sets the total stats for armor, including scaling depending on type of gear.
     */
    protected void setTotalStats(){
        totalHealth =(int) ((baseHealth + bonusHealth) * multiplier);
        totalStrength =(int) ((baseStrength + bonusStrength) * multiplier);
        totalDexterity =(int) ((baseDexterity + bonusDexterity) * multiplier);
        totalIntelligence =(int) ((baseIntelligence + bonusIntelligence) * multiplier);
    }

    public ArmorSlot getArmorSlot() {
        return armorSlot;
    }

    public String getName() {
        return name;
    }

    public int getTotalHealth() {
        return totalHealth;
    }

    public int getTotalStrength() {
        return totalStrength;
    }

    public int getTotalDexterity() {
        return totalDexterity;
    }

    public int getTotalIntelligence() {
        return totalIntelligence;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Item stats for: " + name +
                "\nArmor Type:\t" + armorType +
                "\nSlot:\t\t" + armorSlot +
                "\nBonus HP:\t" + totalHealth +
                "\nBonus STR:\t" + totalStrength +
                "\nBonus DEX:\t" + totalDexterity +
                "\nBonus INT:\t" + totalIntelligence +
                "\nLevel:\t\t" + level;
    }
}
