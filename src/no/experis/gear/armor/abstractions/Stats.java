package no.experis.gear.armor.abstractions;

//Interface for all armorType classes.
public interface Stats {
    void setBaseStats();
    void setBonusStats();
}
