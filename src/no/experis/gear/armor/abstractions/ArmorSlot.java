package no.experis.gear.armor.abstractions;
//The different slots that can equip armor.
public enum ArmorSlot {
    Body, Head, Legs
}
