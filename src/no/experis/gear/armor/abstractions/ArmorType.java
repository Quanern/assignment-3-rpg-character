package no.experis.gear.armor.abstractions;
//The different kinds of material the armor can be made from.
public enum ArmorType {
    Cloth, Leather, Plate
}
