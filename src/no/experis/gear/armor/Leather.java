package no.experis.gear.armor;

import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.armor.abstractions.ArmorType;
import no.experis.gear.armor.abstractions.Stats;
import no.experis.stats.BaseStatsArmor;
import no.experis.stats.BaseStatsArmorLevelUp;

public class Leather extends Armor implements Stats {
    private static ArmorType armorType = ArmorType.Leather; //Sets the armorType to Leather


    /**
     * Constructor to create a new Leather armor piece.
     * @param name name of the Leather armor
     * @param armorSlot which armor slot the armor fits into
     * @param level the level of the armor for bonus stats
     */
    public Leather(String name, ArmorSlot armorSlot, int level){
        super(name, armorType, armorSlot, level);
        setMultiplier(armorSlot);
        setBaseStats();
        setBonusStats();
        setTotalStats();
    }

    /**
     * Sets the base stats for LEATHER armor
     */
    public void setBaseStats(){
        baseHealth = BaseStatsArmor.LEATHER_ARMOR_BASE_HP;
        baseStrength = BaseStatsArmor.LEATHER_ARMOR_BASE_STR;
        baseDexterity = BaseStatsArmor.LEATHER_ARMOR_BASE_DEX;
        baseIntelligence = BaseStatsArmor.LEATHER_ARMOR_BASE_INT;
    }

    /**
     * Sets the bonus stats for LEATHER armor.
     */
    public  void setBonusStats(){
        int addedStatsLevels = (level-1);
        bonusHealth =  addedStatsLevels * BaseStatsArmorLevelUp.LEATHER_ARMOR_LEVEL_UP_HP;
        bonusStrength = addedStatsLevels * BaseStatsArmorLevelUp.LEATHER_ARMOR_LEVEL_UP_STR;
        bonusDexterity = addedStatsLevels *  BaseStatsArmorLevelUp.LEATHER_ARMOR_LEVEL_UP_DEX;
        bonusIntelligence = addedStatsLevels * BaseStatsArmorLevelUp.LEATHER_ARMOR_LEVEL_UP_INT;
    }

    @Override
    public String toString() {
        return "Item stats for: " + name +
                "\nArmor Type:\t" + armorType +
                "\nSlot:\t\t" + armorSlot +
                "\nBonus HP:\t" + totalHealth +
                "\nBonus STR:\t" + totalStrength +
                "\nBonus DEX:\t" + totalDexterity +
                "\nLevel:\t\t" + level;
    }
}
