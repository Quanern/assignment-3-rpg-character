package no.experis.hero.abstractions;

//The different classes a hero can be.
public enum HeroClass {
    Warrior, Ranger, Mage
}
