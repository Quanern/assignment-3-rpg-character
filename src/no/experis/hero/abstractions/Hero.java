package no.experis.hero.abstractions;

import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponSlot;

import java.util.HashMap;

public abstract class Hero {
    private String name; //Name of the hero.
    private HeroClass heroClass; //The hero's class.
    protected int currentHealth; //The hero's current health
    private int maxHealth; //The max hp a hero can have, including gear and level bonuses.
    private int totalStrength; //The total stats the hero has, including gear and level bonuses.
    private int totalDexterity;
    private int totalIntelligence;
    private int experiencePoints; //Experience points the hero has.
    private int experienceTillNextLevel = 100; //How much exp the hero needs to level up.
    private int level = 1; //The hero's level.
    private HashMap<ArmorSlot, Armor> armorEquipment = new HashMap<ArmorSlot, Armor>(); //The hero's equipped armor.
    private HashMap<WeaponSlot, Weapon> weaponEquipment = new HashMap<WeaponSlot, Weapon>(); //The hero's equipped weapon.
    protected int baseHealth; //Base stats of the hero, varies with class.
    protected int baseStrength;
    protected int baseDexterity;
    protected int baseIntelligence;
    protected int levelUpHP; //The amount of stats the hero gains per level up, varies with class.
    protected int levelUpStr;
    protected int levelUpDex;
    protected int levelUpInt;
    private int levelUpHPBonus; //The bonus stats the hero has gained from leveling.
    private int levelUpStrBonus;
    private int levelUpDexBonus;
    private int levelUpIntBonus;
    private int armorHPBonus; //The bonus stats the hero has from armor.
    private int armorStrBonus;
    private int armorDexBonus;
    private int armorIntBonus;


    /**
     * Constructor that takes the heroClass enums as parameter, and then set the stats of the hero
     * depending on what class the hero is.
     * @param name The name you want to call the hero.
     * @param heroClass the class of the new hero.
     */
    public Hero(String name, HeroClass heroClass){
        this.name = name;
        this.heroClass = heroClass;
    }


    /**
     * Function that calculates the hero's stats depending on their class, level and gear.
     *
     */
    protected void updateStats(){
        //Sets the total stats of the hero, including gear and bonus stats from leveling.
        maxHealth = levelUpHPBonus + armorHPBonus + baseHealth;
        totalStrength = levelUpStrBonus + armorStrBonus + baseStrength;
        totalDexterity = levelUpDexBonus + armorDexBonus + baseDexterity;
        totalIntelligence = levelUpIntBonus + armorIntBonus + baseIntelligence;
    }

    /**
     * Function that calculates the hero's bonus stats from leveling.
     */

    private void updateStatsLevelUp(){
        //The amount of bonus stats added depends on the level of the hero.
        int addedStatsLevels = level-1;
        //Sets the bonus stats the hero gets from leveling up into their own variables.
        levelUpHPBonus = addedStatsLevels * levelUpHP;
        levelUpStrBonus = addedStatsLevels * levelUpStr;
        levelUpDexBonus = addedStatsLevels * levelUpDex;
        levelUpIntBonus = addedStatsLevels * levelUpInt;
        updateStats();
    }

    /**
     * Function that calculates the hero's bonus stats from armor.
     */
    public void updateStatsArmor(){
        //Added integers are the increase in each stat depending on their level and gear.
        int addedHP = 0;
        int addedStr = 0;
        int addedDex = 0;
        int addedInt = 0;

        //Checks if the hero has any gear equipped in the different slots, if there is, the added stats will be
        //increased accordingly to the piece of gear.
        if(armorEquipment.containsKey(ArmorSlot.Body)) {
            Armor body = armorEquipment.get(ArmorSlot.Body);
            addedHP += body.getTotalHealth();
            addedStr += body.getTotalStrength();
            addedDex += body.getTotalDexterity();
            addedInt += body.getTotalIntelligence();
        }
        if(armorEquipment.containsKey(ArmorSlot.Head)) {
            Armor head = armorEquipment.get(ArmorSlot.Head);
            addedHP += head.getTotalHealth();
            addedStr += head.getTotalStrength();
            addedDex += head.getTotalDexterity();
            addedInt += head.getTotalIntelligence();
        }
        if(armorEquipment.containsKey(ArmorSlot.Legs)) {
            Armor legs = armorEquipment.get(ArmorSlot.Legs);
            addedHP += legs.getTotalHealth();
            addedStr += legs.getTotalStrength();
            addedDex += legs.getTotalDexterity();
            addedInt += legs.getTotalIntelligence();
        }
        //Sets the armor stat bonus, included scaling.
        armorHPBonus = addedHP;
        armorStrBonus = addedStr;
        armorDexBonus = addedDex;
        armorIntBonus = addedInt;
        updateStats();
    }

    /**
     * Function to give a hero experience points. Function will then recalculate the hero's level and stats.
     * @param exp The amount of experience points you want to give to the hero.
     */
    public void giveExp(int exp){
        //Set the experience points that the hero has to what the hero had + what the hero just got.
        experiencePoints = experiencePoints+exp;
        //Gets the hero's total xp
        int currentXP = experiencePoints;
        int level = 1;
        //Experience points needed for the hero to gain one level.
        int expNeeded = 100;
        //Experience points needed to get to the next level.
        int expNext = 100;
        while (currentXP>=expNeeded){
                //For every level gained, the experience points needed is increased by 10%
                expNext = (int)(1.1*expNext);
                //Adds the needed exp for next level into the total expNeeded to get to the current level.
                expNeeded += (expNext);
                //Sets the current level.
                level++;
        }
        int oldLevel = this.level;
        //Sets how much exp is needed to get to next level.
        experienceTillNextLevel = expNeeded-currentXP;
        //Checks if the hero leveled up.
        if(oldLevel<level){
            //Sets the hero's new level.
            this.level = level;
            //Recalculate the hero's bonus stats after leveling.
            updateStatsLevelUp();
            updateStats();
            //Gives the hero a full hp refill after leveling.
            currentHealth = maxHealth;
        }
    }

    public String getName() {
        return name;
    }
    public int getTotalStrength() {
        return totalStrength;
    }
    public int getTotalDexterity() {
        return totalDexterity;
    }
    public int getTotalIntelligence() {
        return totalIntelligence;
    }
    public HashMap<ArmorSlot, Armor> getArmorEquipment() {
        return armorEquipment;
    }
    public HashMap<WeaponSlot, Weapon> getWeaponEquipment() {
        return weaponEquipment;
    }
    public int getCurrentHealth() {
        return currentHealth;
    }
    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }
    public int getLevel(){
        return level;
    }
    public int getMaxHealth(){
        return maxHealth;
    }

    public String toString() {
        return "Hero details:" +
                "\nName:\t" + name +
                "\nClass:\t" +heroClass +
                "\nHP:\t\t" + currentHealth +"/"+ maxHealth +
                "\nStr:\t" + totalStrength +
                "\nDex:\t" + totalDexterity +
                "\nInt:\t" + totalIntelligence +
                "\nLevel:\t" + level+
                "\nExp needed for next level: " + experienceTillNextLevel;
    }

}
