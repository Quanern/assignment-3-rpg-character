package no.experis.hero.abstractions;

public interface Stats {
    void setBaseStats();
    void setLevelUpStats();
}
