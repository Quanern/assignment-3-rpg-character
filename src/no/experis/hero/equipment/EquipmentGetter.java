package no.experis.hero.equipment;

import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponSlot;
import no.experis.hero.abstractions.Hero;

import java.util.HashMap;

public class EquipmentGetter {
    /**
     * Function to get a list of all the items the hero has equipped.
     * @return String of all the items equipped, or a String that the hero has nothing equipped.
     */
    public String getEquipment(Hero hero){
        StringBuilder stringBuilder = new StringBuilder();
        //Checks if the hashmap for weapon and armor are empty. if either aren't, appends to the StringBuilder
        HashMap<WeaponSlot, Weapon> weaponEquipment = hero.getWeaponEquipment();
        HashMap<ArmorSlot, Armor> armorEquipment = hero.getArmorEquipment();
        if(!weaponEquipment.isEmpty() || !armorEquipment.isEmpty()){
            stringBuilder.append("List of armorEquipment for ");
            stringBuilder.append(hero.getName());
            stringBuilder.append(":");
            //Checks if the hero has a weapon equipped, appends the name of the weapon if a weapon is equipped.
            if(!weaponEquipment.isEmpty()){
                stringBuilder.append("\nWeapon : ");
                stringBuilder.append(weaponEquipment.get(WeaponSlot.Weapon).getName());
            }
            //Checks if the armorEquipment HashMap is empty, if it isn't then it will
            //iterate through the HashMap armorEquipment where the armor is stored,
            //and appends whichever armor is equipped.
            if(!armorEquipment.isEmpty()){
                for (HashMap.Entry<ArmorSlot, Armor> list : armorEquipment.entrySet()){
                    stringBuilder.append("\n");
                    stringBuilder.append(list.getKey());
                    stringBuilder.append(" : ");
                    stringBuilder.append(list.getValue().getName());
                }
            }
        }
        //If the hero has no gear equipped, the String will return this.
        else{
            return hero.getName() + " has nothing equipped.";

        }
        return stringBuilder.toString();
    }

    /**
     * Function to get information about a piece of armor, depending on the armor slot chosen.
     * @param armorSlot decides which armor slot to get information about the piece of gear from.
     * @return Information about the piece of armor. Can print it out due to the toString in the Armor.java class.
     */
    public String getEquipment(Hero hero, ArmorSlot armorSlot){
        //Checks if there is armor equipped in the slot, and returns the armor if there is.
        HashMap<ArmorSlot, Armor> armorEquipment = hero.getArmorEquipment();
        if(armorEquipment.containsKey(armorSlot)) {
            return armorEquipment.get(armorSlot).toString();
        } else {

            return hero.getName() + " doesn't have anything equipped in their " + armorSlot + " slot.";
        }
    }

    /**
     * Function to get information about the weapon the hero has equipped.
     * @param weaponSlot the weapon slot that you want information about. Currently only 1 exists.
     * @return Information about the weapon. Can print it out due to the toString in the Weapon.java class.
     */
    public String getEquipment(Hero hero, WeaponSlot weaponSlot){
        //Checks if there is a weapon equipped, and returns the weapon if there is.
        HashMap<WeaponSlot, Weapon> weaponEquipment = hero.getWeaponEquipment();
        if(weaponEquipment.containsKey(weaponSlot)) {
            return weaponEquipment.get(weaponSlot).toString();
        } else {
            return hero.getName() +" doesn't have any " + weaponSlot +" equipped.";
        }
    }

}
