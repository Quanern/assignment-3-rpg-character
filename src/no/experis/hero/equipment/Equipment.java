package no.experis.hero.equipment;

import no.experis.gear.armor.abstractions.Armor;
import no.experis.gear.armor.abstractions.ArmorSlot;
import no.experis.gear.weapon.abstractions.Weapon;
import no.experis.gear.weapon.abstractions.WeaponSlot;
import no.experis.hero.abstractions.Hero;

import java.util.HashMap;

public class Equipment {

    /**
     * Function that equips a piece of armor to the hero.
     * @param armor the piece of armor you want the hero to equip.
     * @return A String saying what happened when the hero tried to equip the armor.
     */
    public String equip (Hero hero, Armor armor){
        String equip;
        //If the hero is lower level than the piece of armor, then the hero can't equip it.
        HashMap<ArmorSlot, Armor> armorEquipment = hero.getArmorEquipment();
        if(armor.getLevel()>hero.getLevel()){
            equip = hero.getName() + "is only level " + hero.getLevel()  +  ". You must be level " +
                    armor.getLevel() + " or higher to equip this item.";
        }
        else {
            ArmorSlot armorSlot = armor.getArmorSlot();
            //Checks if the hero already has a piece of gear equipped in the slot
            if(armorEquipment.containsKey(armorSlot)){
                Armor oldArmor = armorEquipment.get(armorSlot);
                equip = hero.getName() + " has replaced " + oldArmor.getName() + " with " + armor.getName() +".";
            }
            else {
                equip = hero.getName() + " has equipped " + armor.getName() + " in the " + armorSlot + " slot.";
            }
            //Puts the piece of armor into the HashMap, replacing the old one if there was one.
            armorEquipment.put(armorSlot, armor);
            //Recalculate the bonus stats from armor after equipping the piece of armor.
            hero.updateStatsArmor();
        }
        return equip;
    }

    /**
     * Function that equips the hero with a weapon.
     * @param weapon the weapon you want the hero to equip
     * @return A String saying what happened when the hero tried to equip the weapon.
     */
    public String equip (Hero hero, Weapon weapon){
        String equip;
        HashMap<WeaponSlot, Weapon> weaponEquipment = hero.getWeaponEquipment();
        //If the hero is lower level than the weapon, then the hero can't equip it.
        if(weapon.getLevel() > hero.getLevel()){
            equip = hero.getName() + "is only level " + hero.getLevel()  +  ". You must be level " +
                    weapon.getLevel() + " or higher to equip this item.";
        }
        else {
            //Checks if the hero already has a weapon equipped.
            if(weaponEquipment.containsKey(WeaponSlot.Weapon)){
                Weapon oldWeapon = weaponEquipment.get(WeaponSlot.Weapon);
                equip = hero.getName() + " has replaced " + oldWeapon.getName() + " with " + weapon.getName() +".";
            }
            else {
                equip = hero.getName() + " has equipped " + weapon.getName() +
                        " in the " + WeaponSlot.Weapon + " slot.";
            }
            //Puts the weapon into the HashMap, replacing the old weapon if there was one.
            weaponEquipment.put(WeaponSlot.Weapon, weapon);
        }
        return equip;
    }

    /**
     * Function to unequip a piece of armor from the hero. Recalculates the hero's stats if armor is unequipped.
     * @param armorSlot the armor slot you want to remove.
     * @return A String saying what happened when the hero tried to unequip their armor.
     */
    public String unequip(Hero hero, ArmorSlot armorSlot){
        String unequip;
        HashMap<ArmorSlot, Armor> armorEquipment = hero.getArmorEquipment();
        //Checks if the hero has an armor equipped in the slot.
        if(armorEquipment.containsKey(armorSlot)){
            //Removes the piece of armor from the HashMap.
            armorEquipment.remove(armorSlot);
            unequip = hero.getName() + " has unequipped their " + armorSlot + " armor.";
            //Recalculates bonus stats from armor after removing the piece of armor.
            hero.updateStatsArmor();
        }
        else {
            //No changes to be done if there is nothing to unequip.
            unequip = hero.getName() + " doesn't have anything equipped in the " + armorSlot + " slot.";
        }
        if(hero.getCurrentHealth()> hero.getMaxHealth()){
            hero.setCurrentHealth( hero.getMaxHealth());
        }
        return unequip;
    }

    /**
     * Function to unequip the weapon.
     * @param weaponSlot the weapon slot that you want the hero to remove. Currently only 1 slot that can be
     *                   equipped with a weapon.
     * @return A String saying what happened when the hero tried to unequip their weapon.
     */
    public String unequip(Hero hero, WeaponSlot weaponSlot){
        String unequip;
        HashMap<WeaponSlot, Weapon> weaponEquipment = hero.getWeaponEquipment();
        //Checks if the hero has a weapon equipped.
        if(!weaponEquipment.isEmpty()){
            //Removes the weapon from the HashMap if the hero has one
            weaponEquipment.remove(weaponSlot);
            unequip = hero.getName() + " has unequipped their " + weaponSlot + ".";
        }
        else {
            //No changes if there is nothing to unequip.
            unequip=hero.getName() + " doesn't have any " + weaponSlot + " equipped.";
        }
        return unequip;
    }
}
