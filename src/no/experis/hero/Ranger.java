package no.experis.hero;

import no.experis.hero.abstractions.Hero;
import no.experis.hero.abstractions.HeroClass;
import no.experis.hero.abstractions.Stats;
import no.experis.stats.BaseStatsHero;
import no.experis.stats.BaseStatsHeroLevelUp;

public class Ranger extends Hero implements Stats {
    private static HeroClass heroClass = HeroClass.Ranger; //Sets the HeroClass to Ranger.

    /**
     * Creates a new Ranger, sets it's base stats and it's stat gains from leveling.
     * @param name The name you want to give the Ranger.
     */
    public Ranger(String name){
        super(name, heroClass);
        setBaseStats();
        setLevelUpStats();
    }

    /**
     * Sets the base stats for the Ranger as the pre-set values.
     */
    @Override
    public void setBaseStats(){
        baseHealth = BaseStatsHero.RANGER_BASE_HEALTH;
        baseStrength = BaseStatsHero.RANGER_STRENGTH;
        baseDexterity = BaseStatsHero.RANGER_DEXTERITY;
        baseIntelligence = BaseStatsHero.RANGER_INTELLIGENCE;
        currentHealth = BaseStatsHero.RANGER_BASE_HEALTH;
        updateStats();
    }

    /**
     * Sets the stats the Ranger gain per level up
     */
    public void setLevelUpStats(){
        levelUpHP = BaseStatsHeroLevelUp.RANGER_HEALTH_LEVEL_UP;
        levelUpStr = BaseStatsHeroLevelUp.RANGER_STRENGTH_LEVEL_UP;
        levelUpDex = BaseStatsHeroLevelUp.RANGER_DEXTERITY_LEVEL_UP;
        levelUpInt = BaseStatsHeroLevelUp.RANGER_INTELLIGENCE_LEVEL_UP;
    }
}
