package no.experis.hero;

import no.experis.hero.abstractions.Hero;
import no.experis.hero.abstractions.HeroClass;
import no.experis.hero.abstractions.Stats;
import no.experis.stats.BaseStatsHero;
import no.experis.stats.BaseStatsHeroLevelUp;

public class Warrior extends Hero implements Stats {
    private static HeroClass heroClass = HeroClass.Warrior; //Sets the HeroClass to Warrior

    /**
     * Creates a new Warrior, sets it's base stats and it's stat gains from leveling.
     * @param name The name you want to give the Warrior.
     */
    public Warrior(String name){
        super(name, heroClass);
        setBaseStats();
        setLevelUpStats();
    }

    /**
     * Sets the base stats for the Warrior as the pre-set values.
     */
    @Override
    public void setBaseStats(){
        baseHealth = BaseStatsHero.WARRIOR_BASE_HEALTH;
        baseStrength = BaseStatsHero.WARRIOR_STRENGTH;
        baseDexterity = BaseStatsHero.WARRIOR_DEXTERITY;
        baseIntelligence = BaseStatsHero.WARRIOR_INTELLIGENCE;
        currentHealth = BaseStatsHero.WARRIOR_BASE_HEALTH;
        updateStats();
    }

    /**
     * Sets the stats the Warrior gain per level up
     */
    public void setLevelUpStats(){
        levelUpHP = BaseStatsHeroLevelUp.WARRIOR_HEALTH_LEVEL_UP;
        levelUpStr = BaseStatsHeroLevelUp.WARRIOR_STRENGTH_LEVEL_UP;
        levelUpDex = BaseStatsHeroLevelUp.WARRIOR_DEXTERITY_LEVEL_UP;
        levelUpInt = BaseStatsHeroLevelUp.WARRIOR_INTELLIGENCE_LEVEL_UP;
    }
}
