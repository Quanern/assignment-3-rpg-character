package no.experis.hero;

import no.experis.hero.abstractions.Hero;
import no.experis.hero.abstractions.HeroClass;
import no.experis.hero.abstractions.Stats;
import no.experis.stats.BaseStatsHero;
import no.experis.stats.BaseStatsHeroLevelUp;

public class Mage extends Hero implements Stats {
    private static HeroClass heroClass = HeroClass.Mage; //Sets the HeroClass to Mage

    /**
     * Creates a new Mage, sets it's base stats and it's stat gains from leveling.
     * @param name The name you want to give the Mage.
     */
    public Mage(String name){
        super(name, heroClass);
        setBaseStats();
        setLevelUpStats();
    }
    /**
     * Sets the base stats for the Mage as the pre-set values.
     */
    @Override
    public void setBaseStats(){
        baseHealth = BaseStatsHero.MAGE_BASE_HEALTH;
        baseStrength = BaseStatsHero.MAGE_STRENGTH;
        baseDexterity = BaseStatsHero.MAGE_DEXTERITY;
        baseIntelligence = BaseStatsHero.MAGE_INTELLIGENCE;
        currentHealth = BaseStatsHero.MAGE_BASE_HEALTH;
        updateStats();
    }

    /**
     * Sets the stats the Mage gain per level up
     */
    public void setLevelUpStats(){
        levelUpHP = BaseStatsHeroLevelUp.MAGE_HEALTH_LEVEL_UP;
        levelUpStr = BaseStatsHeroLevelUp.MAGE_STRENGTH_LEVEL_UP;
        levelUpDex = BaseStatsHeroLevelUp.MAGE_DEXTERITY_LEVEL_UP;
        levelUpInt = BaseStatsHeroLevelUp.MAGE_INTELLIGENCE_LEVEL_UP;
    }
}
