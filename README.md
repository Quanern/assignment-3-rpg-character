# Assignment 3 - RPG Character
This is the third assignment in the Noroff upskill course for Java.
In this application you will be able to create heroes and gear. You can give experience points to level up the heroes,
equip them with the gear you made and make them attack. 

## Hero
In this application, a hero can have three different classes, mage, ranger and warrior. Depending on the class, the 
hero will have different base stats and stat gains on level up. Heroes start out at level 1 and have the following
stats:
- Health: The amount of health the hero has.
- Strength: Increases the amount of melee damage the hero does.
- Dexterity: Increases the amount of ranged damage the hero does.
- Intelligence: Increases the amount of magic damage the hero does.

## Gear
Gear is in this application separated into armor and weapon. Armor is further separated into Cloth, Leather or Plate
armor, and they can be either a Head armor, Body armor or Legs armor. Depending on what material the armor is made of,
the armor will have different base stats and stat gains depending on it's level. Depending on what kind of armor it is,
a multiplier will decide the final stats of the armor.
- 1.0 multiplier for Body armor.
- 0.8 multiplier for Head armor.
- 0.6 multiplier for Legs armor.

Since all armor of the same material shares the same base stats and stat gains, Legs will be the weakest of them all,
with only 60% worth of stats compared to Body.

Weapons are separated into what kind of damage they do, magic, ranged and melee. Melee damage will be boosted by the
stat *strength*, magic by the stat *intelligence*, and ranged by the stat *dexterity*. 


## Attack
Heroes can attack target dummies to measure how much damage they will do. The type of damage is reliant on the weapon
type, and it's damage is increased by the hero's stats. Different kind of attacks have different stat multipliers added
to them. If the hero has no weapon equipped, they can't attack.

If brave enough, heroes can attack other heroes, and even kill them. You obviously can not attack back if you're dead.
You can also not hit yourself.

## Equipment
Every hero can equip gear that is equal or lower level than they are. By equipping armor, the hero will gain stats
equal to the stats on the armor, and by unequipping the armor, the stats will decrease accordingly.
Current hp will not increase if the armor equipped increases their max hp, and if the hero un-equips their armor, 
the hero's current hp will be set to the hero's max hp if the current hp is higher than the max.

By equipping a weapon you get the possibility to attack.

## Usage
The demonstration in Main.java will showcase creating a hero, giving a hero exp, creating armor, failing to equip gear
since a hero's level is too low, letting a hero equip gear, unequip gear and attack.

### Hero
New heroes can be made through the HeroFactory, where the getHero()-method will return a new hero to you.
The parameters are the name of the hero and what class the hero is. 

### Gear
New weapons are made through the WeaponFactory, where the getWeapon()-method will return a new weapon to you.
The parameters are the name of the weapon, what kind of weapon it is and it's level.

New armor are made through the ArmorFactory, where the getArmor()-method will return a new piece of armor to you.
The paramaters are the name of the armor, what type the armor is, what slot the armor goes in and it's level.

### Attack
To initiate an attack, you issue one at the AttackFactory, where the getAttack()-method will return you the result of
the attack.
To attack a dummy, the parameter is the hero attacking.
To attack a hero, the parameters are the hero attacking and the hero being attacked.

### Equipment
You can check a hero's current equipment by calling the getEquipment() method from EquipmentGetter.java.
With only the hero as parameter, the name of every piece of gear they have equipped will be printed out. With the 
weapon or armor slot as a second parameter, you will get information about the specific piece of gear.

To equip or un-equip gear, you use the equip() and unequip() methods from Equipment.java. 
Parameters for the methods are the hero doing the (un)equipping, and the gear to be un(equipped).